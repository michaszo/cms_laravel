<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\UpdateUserRequest;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    public function makeAdmin(User $user)
    {
        $user->role = 'admin';
        $user->save();
        return redirect('users')->with('success', $user->name . ' is now admin');
    }

    public function editProfile()
    {
        $user = auth()->user();
        return view('users.edit', compact('user'));
    }

    public function updateProfile(UpdateUserRequest $request)
    {
        $user = auth()->user();

        $user->update([
            'name' => $request->name,
            'about' => $request->about,
        ]);

        return redirect()
            ->back()
            ->with('success', $user->name . ' is now updated.');
    }


}
