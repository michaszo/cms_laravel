@extends('layouts.app')
@section('content')

    <div class="row">
        <div class="col-12 text-center">
            <div class="card card-default">
                <div class="card-header">
                    <h4>edit category</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('categories.update', $category->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <input class="form-control" type="text" name="name" placeholder="category name"
                                   value="{{old('name') ?? $category->name}}">
                        </div>
                        <button class="btn btn-primary" type="submit">edit category</button>
                    </form>
                </div>
            </div>
        </div>

@endsection
