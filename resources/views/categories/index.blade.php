@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <a href="{{route('categories.create')}}">
                        <button class="btn btn-success float-right">new category</button>
                    </a>
                    <h4>Categories:</h4>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        @forelse ($categories as $category)
                            <li class="list-group-item">

                                <span>{{$category->id}}. {{ $category->name }}</span>
                                <span>Post count: {{$category->posts()->count()}}</span>

                                <span class="dropdown">
                                    <button class="btn btn-info btn-sm float-right dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        options
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <form action="{{ route('categories.destroy',$category->id) }}" method="POST">
                                            <a class="dropdown-item"
                                               href="{{ route('categories.edit',$category->id) }}">Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="dropdown-item" onclick="return confirm('Are you sure?')">Delete</button>
                                        </form>
                                    </div>
                                </span>

                            </li>
                        @empty
                            <p>No categories</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row pagination justify-content-center sticky-bottom my-5">
        <div class="">{{$categories}}</div>
    </div>
@endsection


