@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12 text-center">
            <div class="card card-default">
                <div class="card-header">
                    <h4>new category</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('categories.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <input class="form-control" type="text" name="name" placeholder="category name">
                        </div>
                        <button class="btn btn-success" type="submit">add category</button>
                    </form>
                </div>
            </div>
        </div>

@endsection
