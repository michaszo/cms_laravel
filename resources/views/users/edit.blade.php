@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <h4>edit profile</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('users.update_profile')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">name:</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{old('name') ?? $user->name}}">
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="about" class="col-md-4 col-form-label text-md-right">about:</label>
                            <div class="col-md-6">
                                <textarea id="about" type="text" class="form-control" name="about"
                                          >{{old('about') ?? $user->about}}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right"></label>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-success">update profile</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection


