@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <h4>new post</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <input class="form-control" type="text" name="title" placeholder="post name">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="description" placeholder="post description">
                        </div>
                        <div class="form-group">
                            @trix(\App\Post::class, 'post_content')
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" value="{{date('d-m-Y G:i',time())}}"
                                   name="published_at" id="published_at" placeholder="post publish_at">
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="category">category</label>
                            </div>
                            <select class="custom-select" id="category" name="category">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">
                                        {{$category->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="tags">tags (select with pressing shift)</label>
                            </div>
                            <select class="custom-select" multiple id="tags" name="tags[]">
                                @foreach($tags as $tag)
                                    <option value="{{$tag->id}}">
                                        {{$tag->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <input class="form-control" type="file" name="image" placeholder="post image">
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-success" type="submit">add post</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection

