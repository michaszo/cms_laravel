@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <h4>Users:</h4>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        @forelse ($users as $user)
                            <li class="list-group-item">
                                <span>
                                    <img src="{{ Gravatar::src($user->email) }}" class="rounded-left">
                                </span>
                                <span>{{ $user->name }}</span>
                                <span>{{ $user->role }}</span>
                                <span class="dropdown">
                                    <button class="btn btn-info btn-sm float-right dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        options
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                        @if(!$user->isAdmin())
                                            <form action="{{route('users.make_admin', $user->id)}}" method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-success btn-block">make admin</button>
                                            </form>
                                        @endif

                                    </div>
                                </span>

                            </li>
                        @empty
                            <p>No users</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
{{--    <div class="row pagination justify-content-center sticky-bottom my-5">--}}
{{--        <div class="">{{$users}}</div>--}}
{{--    </div>--}}
@endsection


