<div class="col-md-4">
    <ul class="list-group">

        @if(auth()->user()->isAdmin())
            <li class="list-group-item">
                <a href="{{url('users')}}">users</a>
            </li>
        @endif

        <li class="list-group-item">
            <a href="{{url('categories')}}">categories</a>
        </li>
        <li class="list-group-item">
            <a href="{{url('posts')}}">posts</a>
        </li>
        <li class="list-group-item">
            <a href="{{url('trashed_posts')}}">trashed posts</a>
        </li>
        <li class="list-group-item">
            <a href="{{url('tags')}}">tags</a>
        </li>
    </ul>
</div>
