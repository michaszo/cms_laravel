@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <h4>edit post</h4>

                </div>
                <div class="card-body">
                    <form action="{{route('posts.update', $post->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <input class="form-control" type="text" name="title" placeholder="post name"
                                   value="{{old('title') ?? $post->title}}">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="description" placeholder="post description"
                                   value="{{old('description') ?? $post->description}}">
                        </div>
                        <div class="form-group">
                            <input id="post_content" value="" type="hidden" name="post_content">
                            <trix-editor input="post_content">{!! $post->content !!}</trix-editor>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">category</label>
                            </div>
                            <select class="custom-select" id="inputGroupSelect01" name="category">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}"
                                            @if($category->id == $post->category_id)
                                            selected
                                        @endif
                                    >
                                        {{$category->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="tags">tags (select with pressing shift)</label>
                            </div>
                            <select class="custom-select" multiple id="tags" name="tags[]">
                                @foreach($tags as $tag)
                                    <option value="{{$tag->id}}"
                                        @if($post->hasTag($tag->id))
                                            selected
                                        @endif
                                    >
                                        {{$tag->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <input class="form-control" type="text"
                                   value="{{date('d-m-Y G:i', old('published_at') ?? $post->published_at)}}"
                                   name="published_at" id="published_at" placeholder="post publish_at">
                        </div>
                        @if($post->image)
                            <div class="mt-5">
                                <img src="{{asset('storage/'. $post->image)}}" class="img-fluid"
                                     alt="{{$post->description}}">
                            </div>
                        @endif
                        <div class="form-group">
                            <input class="form-control" type="file" name="image" placeholder="post image"
                                   value="{{old('image') ?? $post->image}}">
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-success" type="submit">edit post</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
@endsection


