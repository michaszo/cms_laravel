@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <a href="{{route('posts.create')}}">
                        <button class="btn btn-success float-right">add post</button>
                    </a>
                    <h4>Posts:</h4>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        @forelse ($posts as $post)
                            <li class="list-group-item">

                                <span>{{$post->id}}. {{ $post->title }}</span>
                                <span class="dropdown">
                                    <button class="btn btn-info btn-sm float-right dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        options
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                                            @if(!$post->trashed())
                                            <a class="dropdown-item"
                                               href="{{ route('posts.edit',$post->id) }}">Edit</a>
                                        @else
                                            <form action="{{route('restore_post',$post->id)}}" method="post">
                                                    @csrf
                                                @method('PUT')
                                                    <button class="dropdown-item"
                                                            type="submit">Restore</button>
                                                </form>
                                        @endif
                                        <form action="{{ route('posts.destroy',$post->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="dropdown-item"
                                                    onclick="return confirm('Are you sure?')">
                                                {{$post->trashed() ? 'Delete' : 'Trash'}}
                                            </button>
                                        </form>

                                    </div>
                                </span>

                                @if($post->image)
                                    <div class="mt-5">
                                        <img src="{{asset('storage/'. $post->image)}}" class="img-fluid"
                                             alt="{{$post->description}}">
                                    </div>
                                @endif

                            </li>
                        @empty
                            <p>No posts</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row pagination justify-content-center sticky-bottom my-5">
        <div class="">{{$posts}}</div>
    </div>
@endsection


