@extends('layouts.app')
@section('content')


    <div class="row">
        <div class="col-12 text-center">
            <div class="card card-default">
                <div class="card-header">
                    <h4>new tag</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('tags.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <input class="form-control" type="text" name="name" placeholder="tag name">
                        </div>
                        <button class="btn btn-success" type="submit">add tag</button>
                    </form>
                </div>
            </div>
        </div>

@endsection
