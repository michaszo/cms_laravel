@extends('layouts.app')
@section('content')


    <div class="row">
        <div class="col-12 text-center">
            <div class="card card-default">
                <div class="card-header">
                    <h4>edit tag</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('tags.update', $tag->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <input class="form-control" type="text" name="name" placeholder="tag name"
                                   value="{{old('name') ?? $tag->name}}">
                        </div>
                        <button class="btn btn-primary" type="submit">edit tag</button>
                    </form>
                </div>
            </div>
        </div>

@endsection
