@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card card-default">
                <div class="card-header">
                    <a href="{{route('tags.create')}}">
                        <button class="btn btn-success float-right">new tag</button>
                    </a>
                    <h4>Tags:</h4>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        @forelse ($tags as $tag)
                            <li class="list-group-item">

                                <span>{{$tag->id}}. {{ $tag->name }}</span>
                                <span>Post count: {{$tag->posts()->count()}}</span>

                                <span class="dropdown">
                                    <button class="btn btn-info btn-sm float-right dropdown-toggle" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        options
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <form action="{{ route('tags.destroy',$tag->id) }}" method="POST">
                                            <a class="dropdown-item"
                                               href="{{ route('tags.edit',$tag->id) }}">Edit</a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="dropdown-item" onclick="return confirm('Are you sure?')">Delete</button>
                                        </form>
                                    </div>
                                </span>

                            </li>
                        @empty
                            <p>No tags</p>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row pagination justify-content-center sticky-bottom my-5">
        <div class="">{{$tags}}</div>
    </div>
@endsection


