<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where(
            'email', 'michaszo18@gmail.com'
        )->first();

        if (!$user) {
            User::create([
                'name' => 'Mike',
                'role' => 'admin',
                'email' => 'michaszo18@gmail.com',
                'password' => Hash::make('drops177'),
            ]);
        }
    }
}
