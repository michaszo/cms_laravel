<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(8, 12) as $index) {
            $category = DB::table('categories')->insert([
                'name' => $faker->sentence(6),
            ]);

        }

        foreach (range(10, 24) as $index) {
            $tag = DB::table('tags')->insert([
                'name' => $faker->sentence(1),
            ]);
        }

        foreach (range(1, 5) as $index) {
            $post = DB::table('posts')->insert([
                'id' => $index,
                'title' => $faker->sentence(6),
                'description' => $faker->sentence(12),
                'content' => $faker->paragraph(7),
                'published_at' => $faker->unixTime(),
                'created_at' => $faker->dateTime(),
                'category_id' => random_int(0,8)
            ]);

            foreach (range(0, 5) as $index1) {
                DB::table('post_tag')->insert([
                    'post_id' => $index,
                    'tag_id' => $index1,
                ]);
            }

        }

    }
}
