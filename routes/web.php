<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('categories', 'CategoriesController');

    Route::resource('posts', 'PostsController');

    Route::resource('tags', 'TagsController');

    Route::get('trashed_posts', 'PostsController@trashed');

    Route::put('restore_post/{post}', 'PostsController@restore')->name('restore_post');
});

Route::middleware(['auth', 'admin'])->group(function () {
    Route::get('users', 'UsersController@index')->name('users');
    Route::post('users/{user}/make_admin', 'UsersController@makeAdmin')->name('users.make_admin');
    Route::get('users/edit_profile', 'UsersController@editProfile')->name('user.edit_profile');
    Route::put('users/update_profile', 'UsersController@updateProfile')->name('users.update_profile');
});




